from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(
    bitcoindark=math.Object(
        PARENT=networks.nets['bitcoindark'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares
        SPREAD=30, # blocks
        IDENTIFIER='d0513b03789a213c'.decode('hex'),
        PREFIX='82bb5a0aa8b9efbb'.decode('hex'),
        P2P_PORT=13631,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**32 - 1,
        PERSIST=False,
        WORKER_PORT=13632,
        BOOTSTRAP_ADDRS='btcd.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-btcd',
        VERSION_CHECK=lambda v: True,
    ),
    bitcoindark_testnet=math.Object(
        PARENT=networks.nets['bitcoindark_testnet'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares
        SPREAD=30, # blocks
        IDENTIFIER='d0513b03789a213c'.decode('hex'),
        PREFIX='82bb5a0aa8b9efbb'.decode('hex'),
        P2P_PORT=24714,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**32 - 1,
        PERSIST=False,
        WORKER_PORT=24715,
        BOOTSTRAP_ADDRS=''.split(' '),
        ANNOUNCE_CHANNEL='',
        VERSION_CHECK=lambda v: True,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
